﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp(int spaces)
        {
            if (YPos < City.YMax)
            {
                
                YPos += spaces;
                WritePositionToConsole();
            }
        }

        public override void MoveDown(int spaces)
        {
            if (YPos > 0)
            {
                YPos -= spaces;
               
                WritePositionToConsole();
            }
        }

        public override void MoveRight(int spaces)
        {
            if (XPos < City.XMax)
            {
                XPos += spaces;
                
                WritePositionToConsole();
            }
        }

        public override void MoveLeft(int spaces)
        {
            if (XPos > 0)
            {
                XPos -= spaces;

                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
