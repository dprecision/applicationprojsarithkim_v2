﻿using System;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            while(!passenger.IsAtDestination())
            {
               Tick(car, passenger);
            }

            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            // check to make sure you've reach the passenger and pick them up 
            if (car.XPos == passenger.GetCurrentXPos() && car.YPos == passenger.GetCurrentYPos())
            {
                passenger.GetInCar(car);
            }

            // if there is no passenger then go pick one up 
            if (car.Passenger == null)
            {
                // if your already on the right x axis then move on the y axis
                if(car.XPos == passenger.GetCurrentXPos())
                {
                    if((car.YPos + 2 ) <= passenger.GetCurrentYPos())
                    {
                        car.MoveUp(2);
                        return; 
                    }
                    else if (car.YPos < passenger.GetCurrentYPos())
                    {
                        car.MoveUp(1);
                        return;
                    }
                   
                    if((car.YPos - 2) >= passenger.GetCurrentYPos())
                    {
                        car.MoveDown(2);
                        return;
                    }
                    else if (car.YPos  > passenger.GetCurrentYPos())
                    {
                        car.MoveDown(1);
                        return;
                    }

                }
                else
                {
                    if ((car.XPos + 2) <= passenger.GetCurrentXPos())
                    {
                        car.MoveRight(2);
                        return;
                    }
                    else if(car.XPos  < passenger.GetCurrentXPos())
                    {
                        car.MoveRight(1);
                        return;
                    }
                    if ((car.XPos - 2) >= passenger.GetCurrentXPos())
                    {
                        car.MoveLeft(2);
                        return;
                    }
                    else if (car.XPos  > passenger.GetCurrentXPos())
                    {
                        car.MoveLeft(1);
                        return;
                    }

                }
               
            }

            // if there is a passenger bring them to their destination
            if(car.Passenger != null)
            {
                // if your already on the right x axis then move on the y axis
                if (car.XPos == passenger.DestinationXPos)
                {
                    if ((car.YPos + 2) <= passenger.DestinationYPos)
                    {
                        car.MoveUp(2);
                        return;
                    }
                    else if (car.YPos < passenger.DestinationYPos)
                    {
                        car.MoveUp(1);
                        return;
                    }
                    if ((car.YPos -2) >= passenger.DestinationYPos)
                    {
                        car.MoveDown(2);
                        return;
                    }
                    else
                    {
                        car.MoveDown(1);
                        return;
                    }

                }
                else
                {
                    if ((car.XPos +2) <= passenger.DestinationXPos)
                    {
                        car.MoveRight(2);
                        return;
                    }
                    else if (car.XPos  < passenger.DestinationXPos)
                    {
                        car.MoveRight(1);
                        return;

                    }
                    if ((car.XPos - 1) >= passenger.DestinationXPos)
                    {
                        car.MoveLeft(2);
                        return;
                    }
                    else if (car.XPos  > passenger.DestinationXPos)
                    {
                        car.MoveLeft(1);
                        return;
                    }

                }

            }


        }

    }
}
